// Dependencies
const express = require('express');
const auth = require('../auth.js');

// Routing Component
const router = express.Router();

//Controller
const productController = require('../controllers/productController.js');

// De-Structure Auth
const { verify, verifyAdmin } = auth;

// Add Product
router.post('/add', verify, verifyAdmin, productController.addProduct);

// Get Product
router.get('/get', productController.getAllProducts);

// Retrieve Active Products
router.get('/active', productController.activeProducts);

// Retrieve Single Product
router.get('/:id/view', verify, productController.singleProduct);

// Update Product
router.put('/:id/update', verify, verifyAdmin, productController.updateProduct);

// Archive Product
router.put('/:id/archive', verify, verifyAdmin, productController.archiveProduct);

// Actovate Product
router.put('/:id/activate', verify, verifyAdmin, productController.activateProduct);

// Export Router
module.exports = router;
