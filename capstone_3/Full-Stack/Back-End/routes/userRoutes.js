// [ SECTION ] Dependencies and Modules
const express = require('express');
const auth = require("../auth.js")
const {verify, verifyAdmin} = auth;
const router = express.Router();
const userController = require('../controllers/userController.js');


// Destructure from auth



// [ SECTION ] Routing Component


// check email routes
router.post('/checkEmail', (req, res) => {
	userController.checkEmailExist(req.body).then(resultFromController => res.send(resultFromController));
});

// user registration routes
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// user authentication
//router.post('/login', userController.loginUser);
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

/*// retrieve user details
router.post("/details", verify, (req, res) => {
	userController.getProfile(req.user.id).then(resultFromController => res.send(resultFromController));
})*/

router.get("/details", verify, userController.getProfile);


router.post("/order", verify, userController.order);


router.get('/getOrders', verify, userController.getOrders);

// Reset Password
router.put("/reset-password", verify, userController.resetPassword);

// [ SECTION ] Export Route System
module.exports = router;