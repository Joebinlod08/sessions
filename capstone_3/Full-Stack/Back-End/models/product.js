// [ SECTION ] Dependecies and Modules
const mongoose = require('mongoose');

// [ SECTION ] Schema/Blueprint
const productSchema = new mongoose.Schema({
		name: {
		  type: String,
		  required: [true, 'Please add a name'],
		},
	  
		brand: {
		  type: String,
		   required: [true, 'Please add a brand'],
		},
	  
		description: {
		  type: String,
		  required: [true, 'Please add a description'],
		},
		price: {
		  type: Number,
		  required: [true, 'Please add a price'],
		},
		isActive: {
		  type: Boolean,
		  default: true,
		},
		createdOn: {
		  type: Date,
		  default: new Date(),
		},
	  });

// [ SECTION ] Model
module.exports = mongoose.model('Products', productSchema);