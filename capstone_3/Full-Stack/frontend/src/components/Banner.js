import { Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

// data is received through parameter
export default function Banner({data}) {

	// Object destructuring
    const {title, content, destination, label} = data;

    return (
        <Row>
            <Col className="banner p-5 text-center">
                <h1>{title}</h1>
                <p>{content}</p>
                <Link className="btn btn-primary" to={destination}>{label}</Link>
            </Col>
        </Row>
    )
}