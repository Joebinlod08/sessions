import React from 'react';

const CarouselImage1 = () => {
  return (
    <div className="image-gallery">
      <img className='img-fluid' src={require('.//image/red horse.jpg')} alt="Red Horse" />
    </div>
  );
};

export default CarouselImage1;
