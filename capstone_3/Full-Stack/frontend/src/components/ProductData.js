import { useContext, useEffect, useState } from 'react';
import UserContext from '../context/UserContext';

//Pages
import ProductCard from './ProductCard';
import ProductDash from './ProductDash';

function ProductData() {
  const { user } = useContext(UserContext);

  // State that will be used to store the products retrieved from the database
  const [products, setProducts] = useState([]);
  const link = user.isAdmin === true ? 'all' : 'active';
  const fetchData = () => {
    fetch(`https://capstone-back-end-sf3x.onrender.com/products/${link}`)
      .then((res) => res.json())
      .then((data) => {
        if (data.state === false) {
          setProducts(data.state);
        } else {
          // Sets the "products" state to map the data retrieved from the fetch request into several "Product Data" components
          setProducts(data);
        }
      });
  };

  // Retrieves the products from the database upon initial render of the "Courses" component
  useEffect(() => {
    fetchData();
  });
  return (
    <>
      {user.isAdmin === true ? (
        <ProductDash productsData={products} fetchData={fetchData} />
      ) : (
        <ProductCard productsData={products} />
      )}
    </>
  );
}

export default ProductData;