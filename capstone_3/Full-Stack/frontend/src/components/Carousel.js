import { useState } from 'react';
import Carousel from 'react-bootstrap/Carousel';
import CarouselImage3 from './CarouselImage3'
import CarouselImage2 from './CarouselImage2'
import CarouselImage1 from './CarouselImage1'
import { Container, Col, Row } from 'react-bootstrap';
function ControlledCarousel() {
  const [index, setIndex] = useState(0);

  const handleSelect = (selectedIndex) => {
    setIndex(selectedIndex);
  };

  return (

    <Container className='mb-5'>
       <Row className="justify-content-center">
       <Col xs={6} md={5}>
    <Carousel activeIndex={index} onSelect={handleSelect}>
      <Carousel.Item>
        <CarouselImage1 text="First slide" />
        <Carousel.Caption>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <CarouselImage2 text="Second slide" />
        <Carousel.Caption>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <CarouselImage3 text="Third slide" />
        <Carousel.Caption>
          <h3></h3>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
    </Col>
    </Row>
    </Container>
  );
}

export default ControlledCarousel;