import {useContext } from 'react';
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
// import Form from 'react-bootstrap/Form';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';
// import Button from 'react-bootstrap/Button';

export default function AppNavbar() {


    const { user } = useContext(UserContext);

    return(
        <Navbar className='navbar' expand="md">
            <Container fluid>
                <Navbar.Brand as={Link} to="/">Drunken Store</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav  className="me-auto my-1 my-lg-0"
                     navbarScroll>
                        <Nav.Link as={NavLink} to="/" exact>Home</Nav.Link>
                        <Nav.Link as={NavLink} to="/products" exact>Product</Nav.Link>

                        {(user.id !== null) ? 

                                user.isAdmin 
                                ?
                                <>
                                    <Nav.Link as={Link} to="/addProduct">Add Product</Nav.Link>
                                    <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
                                </>
                                :
                                <>
                                    <Nav.Link as={Link} to="/profile">Profile</Nav.Link>
                                    <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
                                </>
                            : 
                                <>
                                    <Nav.Link as={Link} to="/login">Login</Nav.Link>
                                    <Nav.Link as={Link} to="/register">Register</Nav.Link>
                            
                                </>

                        }
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
        )
}