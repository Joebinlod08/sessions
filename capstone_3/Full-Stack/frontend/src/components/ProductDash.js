import { useEffect, useState } from 'react';
import { Col, Row, Table } from 'react-bootstrap';

import ProductFeature from '../components/ProductFeature';
import ProductState from '../components/ProductState';
import ProductUpdate from '../components/ProductUpdate';

function ProductDash({ productsData, fetchData }) {
  const [products, setProducts] = useState([]);
  useEffect(() => {
    const productsArr =
      productsData === false
        ? nulls
        : productsData.map((prod) => {
            return (
              <tr key={prod._id}>
                <td>{prod._id}</td>
                <td>{prod.sku}</td>
                <td>{prod.name}</td>
                <td>{prod.description}</td>
                <td>{prod.marginPrice * 100}%</td>
                <td>{prod.supplier.map((details) => details.supplierPrice)}</td>
                <td>{prod.price}</td>
                <td>{prod.quantity}</td>
                <td className={prod.isActive ? 'text-success' : 'text-danger'}>
                  {prod.isActive ? 'Available' : 'Unavailable'}
                </td>
                <td className={prod.featured ? 'text-info' : 'text-'}>
                  {prod.featured ? 'Featured' : 'Unfeatured'}
                </td>
                <td>
                  <ProductUpdate product={prod._id} fetchData={fetchData} />
                </td>
                <td>
                  <ProductFeature product={prod._id} fetchData={fetchData} featured={prod.featured} />
                </td>
                <td>
                  <ProductState product={prod._id} isActive={prod.isActive} fetchData={fetchData} />
                </td>
              </tr>
            );
          });

    setProducts(productsArr);
  }, [productsData, fetchData]);

  return productsData === false ? (
    <>
      <Row>
        <Col>
          <h1 className="text-center">NO PRODUCTS FOUND</h1>
        </Col>
      </Row>
    </>
  ) : (
    <>
      <Row>
        <Col className="col-12 px-5">
          <h1 className="text-center my-4"> Admin Dashboard</h1>

          <Table striped bordered hover responsive>
            <thead>
              <tr className="text-center">
                <th>ID</th>
                <th>Name</th>
                <th>Description</th>
                <th>Sell Price</th>
                <th>Quantity</th>
                <th>Availability</th>
                <th colSpan="3">Actions</th>
              </tr>
            </thead>

            <tbody>{products}</tbody>
          </Table>
        </Col>
      </Row>
    </>
  );
}

export default ProductDash;
