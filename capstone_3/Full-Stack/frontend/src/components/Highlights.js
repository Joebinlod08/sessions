import { Col, Card } from 'react-bootstrap';

export default function Hightlights(){
	return (
			<Col className='justify-content-center' >
				<Card className="cardHighlight p-12 text-center">
			      <Card.Body>
			        <Card.Title>
			        	<h2>Your Liquor Cart</h2>
			        </Card.Title>
			        <Card.Text>
			         Your one-stop shop for premium liquors and spirits. Add your favorite beverages to your cart and get ready to toast to unforgettable moments. Cheers to exceptional taste and quality!
			        </Card.Text>
			      </Card.Body>
			    </Card>
			</Col>
	)
}