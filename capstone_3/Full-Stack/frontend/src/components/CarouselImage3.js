import React from 'react';

const CarouselImage = () => {
  return (
    <div className="image-gallery">
      <img className='img-fluid' src={require('.//image/empi.jpg')} alt="Red Horse" />
    </div>
  );
};

export default CarouselImage;