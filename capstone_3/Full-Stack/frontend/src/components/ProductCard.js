import PropTypes from 'prop-types';
import { Button, Card, Col, Row } from 'react-bootstrap';
// import coursesData from '../data/coursesData';

export default function ProductCard({ productProp }) {
  const { name, description, price } = productProp;

  return (
    <Row className="mt-3 mb-3">
      <Col>
        <Card className="cardHighlight p-3">
          <Card.Body>
            <Card.Title>
              <h2>{name}</h2>
            </Card.Title>
            <Card.Subtitle>
              <strong>Description:</strong>
            </Card.Subtitle>
            <Card.Text>{description}</Card.Text>
            <Card.Subtitle>
              <strong>Price:</strong>
            </Card.Subtitle>
            <Card.Text>PhP {price}</Card.Text>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}

// Checking props if the data is correct
ProductCard.propType = {
  course: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
  }),
};
