
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import Carousel from '../components/Carousel'

export default function Home() {

    const data = {
    	/* textual contents*/
        title: "YOUR NUMBER 1 LIQOUR STORE",
        content: "SUSUKA, PERO DI SUSUKO",
        /* buttons */
        destination: "/login",
        label: "Tagay now!"
    }

    return (
        
        <>
            <Banner data={data}/>
            <Carousel />
            <Highlights />
           
        </>
    )
}