import { useEffect, useState, useContext } from 'react';
// import ProductCard from '../components/ProductCard';
// import productsData from '../data/productsData';
import UserContext from '../UserContext';
import UserView from '../components/UserView';
import AdminView from '../pages/AdminView';

export default function Products() {

    const { user } = useContext(UserContext);

    const [products, setProducts] = useState([]);


    const fetchData = () => {
        fetch(`https://capstone-back-end-sf3x.onrender.com/products/get`)
        .then(res => res.json())
        .then(data => {
            
            console.log(data);
            setProducts(data);

        });
    }
    useEffect(() => {

        fetchData()

    }, []);

    return(
        <>
            {
                (user.isAdmin === true) ?
                    <AdminView productsData={products} fetchData={fetchData} />

                    :

                    <UserView productsData={products} />

            }
        </>
    )
}