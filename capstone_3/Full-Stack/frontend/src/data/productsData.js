const productsData = [
	{
		id: "Acl01",
		name: "Red Horse Beer",
		brand: "San Miguel Brewery",
		type: "Alcoholic Beverage",
		description: "Red Horse Beer is a strong, high-alcohol lager brewed by San Miguel Brewery in the Philippines. It has a distinctively robust flavor and a higher alcohol content than regular beers.",
		price: 2.99,
		isActive: true
	  },
	{
		id: "wdc002",
		name: "Pyton - Django",
		description: "Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum hendrerit feugiat neque, vel tincidunt ex bibendum sit amet. Sed maximus elit purus, eu facilisis purus convallis bibendum. Phasellus scelerisque elit et feugiat placerat. Maecenas pellentesque purus augue. Aenean convallis nec mauris vel vestibulum. Aenean nec blandit nisi, non mollis nisi. Proin sit amet erat urna.",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java - Springboot",
		description: "Curabitur vitae enim gravida, eleifend purus eget, interdum erat. Mauris sit amet feugiat libero, ac sollicitudin dolor. Fusce sed nunc nec erat pulvinar fermentum a ac eros. Mauris vitae enim bibendum, pellentesque eros viverra, finibus sem. Mauris justo ex, pharetra in sapien pellentesque, placerat tincidunt diam. Proin pulvinar est sit amet urna blandit, a ornare nunc tempor. Cras cursus, justo in euismod finibus, metus ante posuere nunc, sit amet convallis arcu lacus non nibh.",
		price: 55000,
		onOffer: true
	}
]

export default productsData;