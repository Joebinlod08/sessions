import { useEffect, useState} from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
// import './Banner.css'
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
import Products from './pages/Products';
import ProductView from './pages/ProductView';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import AddProduct from './pages/AddProduct';
import Profile from './pages/Profile';
import './App.css';
import { UserProvider } from './UserContext';

function App() {


    const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

    // Function for clearing localStorage on logout
    const unsetUser = () => {

      localStorage.clear();

    };

    // Because our user state's values are reset to null every time the user reloads the page (thus logging the user out), we want to use React's useEffect hook to fetch the logged-in user's details when the page is reloaded. By using the token saved in localStorage when a user logs in, we can fetch the their data from the database, and re-set the user state values back to the user's details.
    useEffect(() => {

    console.log(user);
    fetch(`ttps://capstone-back-end-sf3x.onrender.com/users/details`, {
      headers: {
        Authorization: `Bearer ${ localStorage.getItem('token') }`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      // Set the user states values with the user details upon successful login.
      if (typeof data._id !== "undefined") {

        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        });

      // Else set the user states to the initial values
      } else {

        setUser({
          id: null,
          isAdmin: null
        });

      }

    })

    }, []);

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
        <Container>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/products" element={<Products />} />
            <Route path="/products/:productID" element={<ProductView />}/>
            <Route path="/logout" element={<Logout />} />
            <Route path="/addProduct" element={<AddProduct />} />
            <Route path="/profile" element={<Profile />} />
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;