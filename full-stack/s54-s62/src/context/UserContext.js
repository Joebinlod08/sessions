import React from 'react';

// Creates a context object for sharing data between components
const UserContext = React.createContext();

// UserProvider allows to provide a value to the context object (UserContext)
export const UserProvider = UserContext.Provider;

export default UserContext;
