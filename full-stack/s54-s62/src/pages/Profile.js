import { useContext } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
// Context Component
import userContext from '../context/UserContext';

export default function Profile() {
  // Context Hooks
  const { user } = useContext(userContext);
  // const showData = fetch('http://localhost:4000/users/').then(function (response) {
  //   return response.json();
  // });
  // console.log(showData);
  return user.id !== null ? (
    <Container className="bg-primary text-white">
      <Row className="p-5">
        <Col>
          <h1>Profile</h1>
          <h3>James Dela Cruz</h3>
          <hr></hr>
          <h4>Contacts</h4>
          <ul>
            <li>Email: jamesDC@mail.com</li>
            <li>Mobile No.: 09211231234</li>
          </ul>
        </Col>
      </Row>
    </Container>
  ) : (
    <Navigate to="/login" />
  );
}
