import { useContext, useEffect, useState } from 'react';
import { Button, Form } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
// Context Component
import UserContext from '../context/UserContext';

export default function Register() {
  // State Hooks to store values of the input fields
  // Syntaax: [stateVaiable, setterFunction] = useState('')
  // Note: setterFunction is to update the state value
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [mobileNo, setMobileNo] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [isActive, setIsActive] = useState(false);
  console.log(firstName);
  console.log(lastName);
  console.log(mobileNo);
  console.log(email);
  console.log(password);
  console.log(confirmPassword);
  console.log(isActive);

  const { user } = useContext(UserContext);
  function registerUser(e) {
    // Prevents the default behaviour durring submission which is page redirection via form submission
    e.preventDefault();
    fetch('http://localhost:4000/users/register', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        email: email,
        mobileNo: mobileNo,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data) {
          setFirstName('');
          setLastName('');
          setEmail('');
          setMobileNo('');
          setPassword('');
          setConfirmPassword('');
          alert('User Registered Successfully');
        } else {
          alert('Please try again!');
        }
      });
  }

  /*
    Hook Effect allows you to perform side effects in your components
    Syntax:
    useEffect(() => {
      condition and actions
    }, [dependencies])

  */
  useEffect(() => {
    if (
      firstName !== '' &&
      lastName !== '' &&
      mobileNo !== '' &&
      email !== '' &&
      password !== '' &&
      confirmPassword !== '' &&
      password === confirmPassword &&
      mobileNo.length === 11
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, mobileNo, email, password, confirmPassword]);

  /*
    Example:
    firstName = "Glenn";
    setFirstName('Glenn Wil')
  */
  return user.id !== null ? (
    <Navigate to="/courses" />
  ) : (
    <Form onSubmit={(e) => registerUser(e)}>
      <h1 className="my-5 text-center">Register</h1>
      <Form.Group>
        <Form.Label>First Name:</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter First Name"
          required
          value={firstName}
          onChange={(e) => {
            setFirstName(e.target.value);
          }}
        />
      </Form.Group>
      <Form.Group>
        <Form.Label>Last Name:</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter Last Name"
          value={lastName}
          onChange={(e) => {
            setLastName(e.target.value);
          }}
          required
        />
      </Form.Group>
      <Form.Group>
        <Form.Label>Email:</Form.Label>
        <Form.Control
          type="email"
          placeholder="Enter Email"
          value={email}
          onChange={(e) => {
            setEmail(e.target.value);
          }}
          required
        />
      </Form.Group>
      <Form.Group>
        <Form.Label>Mobile No:</Form.Label>
        <Form.Control
          type="number"
          placeholder="Enter 11 Digit No."
          value={mobileNo}
          onChange={(e) => {
            setMobileNo(e.target.value);
          }}
          required
        />
      </Form.Group>
      <Form.Group>
        <Form.Label>Password:</Form.Label>
        <Form.Control
          type="password"
          placeholder="Enter Password"
          value={password}
          onChange={(e) => {
            setPassword(e.target.value);
          }}
          required
        />
      </Form.Group>
      <Form.Group>
        <Form.Label>Confirm Password:</Form.Label>
        <Form.Control
          type="password"
          placeholder="Confirm Password"
          value={confirmPassword}
          onChange={(e) => {
            setConfirmPassword(e.target.value);
          }}
          required
        />
      </Form.Group>
      <Form.Group className="mt-3">
        {/* 
        Conditional Rendering
        */}
        {/* {isActive === true ? (
          <Button variant="primary" type="submit">
            Submit
          </Button>
        ) : (
          <Button variant="primary" type="submit" disabled>
            Submit
          </Button>
        )} */}
        <Button variant="primary" type="submit" disabled={isActive === false}>
          Submit
        </Button>
      </Form.Group>
    </Form>
  );
}
