import { useContext, useEffect } from 'react';
import { Navigate } from 'react-router-dom';

// Context Component
import UserContext from '../context/UserContext';

export default function Logout() {
  const { setUser, unsetUser } = useContext(UserContext);
  // localStorage.clear();
  unsetUser();
  useEffect(() =>
    setUser({
      id: null,
      isAdmin: null,
      access: null,
    })
  );
  // Redirect to login page
  return <Navigate to="/login" />;
}
