const coursesData = [
  {
    id: 'wdc001',
    name: 'PHP - Laravel',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc quis est eros. Nullam vitae luctus erat. Quisque id eros mi. Ut id nisl porttitor, tincidunt sem nec, consequat velit. Phasellus eu lacinia mauris, sed laoreet sapien. Nam vel lobortis purus. Duis quis eros neque. Ut ac sem leo.',
    price: 45000,
    onOffer: true,
  },
  {
    id: 'wdc002',
    name: 'Python - Django',
    description:
      'Mauris tincidunt in leo ac hendrerit. Praesent egestas velit quis neque posuere sollicitudin. Donec tristique justo in tempor luctus. Nulla sodales mauris est, quis porttitor dui tempus vel. Nam id nulla faucibus, ultricies felis lobortis, semper dolor. Ut non felis elit.',
    price: 50000,
    onOffer: true,
  },
  {
    id: 'wdc003',
    name: 'Java - Springboot',
    description:
      'Maecenas aliquet orci ut lacinia consequat. Phasellus fermentum felis quis velit lobortis, id tristique erat bibendum. Nulla at ullamcorper ante. Praesent auctor leo ut sem venenatis porta. Vestibulum mauris magna, placerat pharetra interdum quis, rhoncus non leo.',
    price: 55000,
    onOffer: true,
  },
];

export default coursesData;
