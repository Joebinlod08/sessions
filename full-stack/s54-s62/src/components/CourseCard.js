import PropTypes from 'prop-types';
import { useState } from 'react';
import { Button, Card, Col, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';
// import coursesData from '../data/coursesData';

export default function CourseCard({ courseProp }) {
  const { _id, name, description, price } = courseProp;
  // Use the state hook in this compnent to be able to store its state
  /*
    Syntax:
    const [getter, setter] = useState(initialGetterValue);
  */
  // const [count, setCount] = useState(0);
  // const [seat, setSeat] = useState(30);

  // Function that keeps track of the enrolless for a course
  // function enroll() {
  //   if (seat === 0) {
  //     alert('No more seats.');
  //   } else {
  //     setCount(count + 1);
  //     setSeat(seat - 1);
  //   }
  // }

  return (
    <Row className="mt-3 mb-3">
      <Col>
        <Card className="cardHighlight p-3">
          <Card.Body>
            <Card.Title>
              <h2>{name}</h2>
            </Card.Title>
            <Card.Subtitle>
              <strong>Description:</strong>
            </Card.Subtitle>
            <Card.Text>{description}</Card.Text>
            <Link className="btn btn-primary" to={`/courses/${_id}`}>
              Details
            </Link>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}

// Checking props if the data is correct
CourseCard.propType = {
  course: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
  }),
};
