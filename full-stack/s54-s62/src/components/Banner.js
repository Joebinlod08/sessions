import { Button, Col, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';
// import Button from 'react-bootstrap/Button';
// import Col from 'react-bootstrap/Col';
// import Row from 'react-bootstrap/Row';

export default function Banner({ bannerProp }) {
  const { title, message, text, link } = bannerProp;
  return (
    <Row>
      <Col className="p-5 text-center">
        <h1>{title}</h1>
        <p>{message}</p>
        <Link to={link}>
          <Button variant="primary">{text}</Button>
        </Link>
      </Col>
    </Row>
  );
}
