let collection = [];
// Write the queue functions below.

function print(){
    return collection;
}
function enqueue(item) {
    collection.push(item);  
    return collection; 
}
function dequeue() {   
    collection.shift()
    return collection
}
function front() {
    if (collection.length === 0) {
        return null; 
    }
    return collection[0];
}

function size() {
    return collection.length; 
}
function isEmpty() {
    return collection.length === 0; 
}





// Export create queue functions below.
module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};