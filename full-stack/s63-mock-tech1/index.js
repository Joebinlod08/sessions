function countLetter(letter, sentence) {

    // Check first whether the letter is a single character.
    if (typeof letter === 'string' && letter.length === 1) {
        let count = 0;

    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
        for (let i = 0; i < sentence.length; i++) {
            if (sentence[i] === letter) {
                count++;
            }
        }
 // If letter is invalid, return undefined
        return count;
    } else {

        return undefined;
    }
}


function isIsogram(text) {
    text = text.toLowerCase();
    const seen = new Set();


    for (let i = 0; i < text.length; i++) {
        const char = text[i];

        if (seen.has(char)) {
            return false;
        }
        seen.add(char);
    }
    return true;
}

function purchase(age, price) {
    if (age < 13) {
        // Return undefined for people aged below 13.
        return undefined;
    } else if ((age >= 13 && age <= 21) || age >= 65) {
        // Apply a 20% discount for students aged 13 to 21 and senior citizens.
        const discountedPrice = price * 0.8;
        const roundedPrice = discountedPrice.toFixed(2);
        return roundedPrice.toString()
    } else {
        // For people aged 22 to 64, simply round off the price.
        const roundedPrice = price.toFixed(2);
        // Return the rounded price as a string.
        return roundedPrice.toString();
    }
}
function findHotCategories(items) {
    const hotCategories = new Set();
    for (const item of items) {
        if (item.stocks === 0) {
            hotCategories.add(item.category);
        }
    }
    const result = Array.from(hotCategories);
    return result;
}

const items = [
    { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' },
    { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' },
    { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' },
    { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' },
    { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }
];


function findFlyingVoters(candidateA, candidateB) {

       // Find voters who voted for both candidate A and candidate B.
    const commonVoters = candidateA.filter(voter => candidateB.includes(voter));

    return commonVoters;
}

        const candidateA = ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m'];
        const candidateB = ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l'];

const flyingVoters = findFlyingVoters(candidateA, candidateB);

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};