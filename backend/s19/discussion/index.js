console.log ("Hello World?");

// (SECTION) Syntax, Statement and Comments //
	// JS Statements usually end with semicolon (;)//

	// Comments:
	
	// There are two types of comments:
	// 1. The Single-line Comment denoted by //
	// 2. The Multiple-line comment denoted by /**/, double asterisk in the middle

//  [ SECTION ] Variables
	// It is used to contain
	// Any information that is used by an application is stored in what we call a "memory"

	// Declaring variables

	// Declaring Variables - tells us our devices that a variable name is created

	let myVariableNumber;     // A naming convension
	console.log("myVariableNumber");
	console.log(myVariableNumber);
	// let	myVariableNumber
	// let my-variable-number
	// let my_variable_number

	// Syntax
		// let/const/var variableName

	// Declaring and Initializzing Variables

	let productName ="desktop computer";
	console.log(productName);

	let	productPrice1 = "18999";
	let	productPrice = 18999;
	console.log(productPrice1);
	console.log(productPrice);

	// const, impossible to reassign
	const interest = 3.539; 

	// Reassignign variable value

	productName = "Laptop";
	console.log(productName)

	// interest = 4.239 // will return an error
	// console.log(interest)

 	// Reassigning Variables Vs Initalizing Variables

 	// Declate the variables

 	myVariableNumber = 2023;
 	console.log(myVariableNumber);

 	//Multiple  Variable Declarations

 	let productCode = 'DC017';
 	const productBrand = 'Dell';

 	console.log(productCode, productBrand);

 	// [ SECTION ] Data Types

 	let	country = "Philippines";
 	let province = 'Metro Manila';

 	// Concatenation Strings
 	let fullAddress = province + "," + country
 	console.log(fullAddress)
 	// console.log("Metro Manila, Philippines")

 	let	greeting = "I live in the " + country;
 	console.log(greeting);

 	let mailAddress = "Metro Manila\n\nPhilippines";
 	console.log(mailAddress);

 	let message = "John's employees went home early";
 	console.log(message);

 	message = 'John\'s employees went home early again!';
 	console.log(message);

 	// Numbers
 	let	headcount = 26;
 	console.log(headcount);


 	// Decimal Numbers/Fractions
 	let	grade = 98.7;
 	console.log(grade);

 	// Exponential Notation
 	let	planetDistance = 2e10;
 	console.log(planetDistance);

 	// Combining text and strings
 	console.log("John's grade last quarter " + grade);


 	// Boolean
 	// Returns true or false

 	let isMarried = false;
 	let	inGoodConduct = true;

 	console.log("isMarried: " + isMarried);
 	console.log("inGoodConduct: " + inGoodConduct);

 	// Arrays

 	let grades = [ 98.7, 92.1, 90.2, 94.6 ]
 	console.log(grades);
 	console.log(grades[0]);
 	console.log(grades[1]);
 	console.log(grades[2]);
 	console.log(grades[3]);
 	console.log(grades[4]);


 	// Different Data types

 	let details = [ "John", "Smith", 32, true]
 	console.log(details[0]);
 	console.log(details[1]);
 	console.log(details[2]);
 	console.log(details[3]);
 	console.log(details[4]);

 	// Objects
 	let	person = {
 		fullName: "Jua Dela Cruz",
 		age: 35,
 		isMarried: false,
 		contact: ["09123456789", "09987654321"],
 		address:  {
 			houseNumber: "345",
 			city: "Manila"
 		}
 	};

 	console.log(person)
 	console.log(person.fullName);
 	console.log(person.age);
 	console.log(person.isMarried);
	console.log(person.contact[0]);
	console.log(person.contact[1]);
 	console.log(person.address.houseNumber);
 	console.log(person.address.city);


 	let	arrays = [
 		["hey", "hey1", "hey2"],
 		["hey", "hey1", "hey2"],
 

 	];

 	console.log(arrays)

 	let	myGrades = {
 		firstGrading: 98.7,
 		secondGradig: 92.1,
 		thirdGrading: 90.2,
 		fourthGrading: 94.6

 	};

 	console.log(myGrades)

 	// Typeof Operator
 		// It will determine the type of the data.
 	console.log(typeof	myGrades);
 	console.log(typeof arrays);
 	console.log(typeof greeting);

 	// Null
 	let spouse = null;
 	let	myNumber = 0;
 	let myString = ''


 	console.log(spouse);
 	console.log(myNumber);
 	console.log(myString)

 	//Undefined
 	let	fullName;
 	console.log(fullName);


