// Dependencies
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

// Settings
const app = express();
const PORT = 4000;

// Middleware
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors());

// Routes
const userRoutes = require('./routes/userRoutes.js');
const productRoutes = require('./routes/productRoutes.js');
const orderRoutes = require('./routes/orderRoutes.js');

// Connection
mongoose.connect(
  'mongodb+srv://admin:admin123@b305.kiyrj33.mongodb.net/capstone_2?retryWrites=true&w=majority',
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

// Connection Check
const dbChecker = mongoose.connection;
dbChecker.on('error', console.error.bind(console, 'MongoDB connection error:'));
dbChecker.once('open', () => {
  console.log('Connected to Atlas MongoDB');
});

// Routes
app.use('/users', userRoutes);
app.use('/products', productRoutes);
app.use('/orders', orderRoutes);

// Module Listening
if (require.main === module) {
  app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`);
  });
}

module.exports = app;
