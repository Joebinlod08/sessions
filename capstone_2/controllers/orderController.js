// Dependencies
const Orders = require('../models/Order.js');
const auth = require('../auth.js');
const bcrypt = require('bcrypt');

// Create a new order
const createOrder = async (req, res) => {
  try {
    const newOrder = new Orders({
      userId: req.body.userId,
      product: req.body.product
    });

    // Save the order to the database
    await newOrder.save().then((order, err) => {
        if (err) {
          res.status(500).send('Error');
        } else {
          res.status(200).json({ message: 'Create Order', Info: order });
        }
      });
    } catch (err) {
      res.status(500).send('Error');
    }
  };

// Get all orders
const getAllOrders = async (req, res) => {
  try {
    const orders = await Orders.find();

    res.status(200).json(orders);
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

// Get an order by ID
const getOrderById = async (req, res) => {
  try {
    const { id } = req.params;
    const orders = await Orders.findById(id);

    if (!order) {
      return res.status(404).json({ error: 'Order not found' });
    }
    res.status(200).json(order);
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

// Update an order by ID
const updateOrderById = async (req, res) => {
  try {
    const { id } = req.params;
    const { userId, orders } = req.body;

    const updatedOrder = await Order.findByIdAndUpdate(
      id,
      {
        userId: req.body.userId,
        product: req.body.product
      },
      { new: true }
    );

    if (!updatedOrder) {
      return res.status(404).json({ error: 'Order not found' });
    }

    res.status(200).json({ message: 'Order updated successfully', order: updatedOrder });
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

// Delete an order by ID
const deleteOrderById = async (req, res) => {
  try {
    const { id } = req.params;

    const deletedOrder = await Order.findByIdAndDelete(id);

    if (!deletedOrder) {
      return res.status(404).json({ error: 'Order not found' });
    }

    res.status(200).json({ message: 'Order deleted successfully', order: deletedOrder });
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

module.exports = {
  createOrder,
  getAllOrders,
  getOrderById,
  updateOrderById,
  deleteOrderById,
};
