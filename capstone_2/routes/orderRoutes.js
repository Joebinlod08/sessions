const express = require('express');
const router = express.Router();
const auth = require('../auth.js');

const orderController = require('../controllers/orderController');

// De-Structure Auth
const { verify, verifyAdmin } = auth;

//const { verify, verifyAdmin } = auth;
router.post('/create', verify, verifyAdmin, orderController.createOrder);

// Route to get all orders
// router.get('/order', orderController.getAllOrders);

// // Route to get an order by ID
// router.get('/order/:id', orderController.getOrderById);

// // Route to update an order by ID
// router.put('/order/:id', orderController.updateOrderById);

// // Route to delete an order by ID
// router.delete('/order/:id', orderController.deleteOrderById);

module.exports = router;