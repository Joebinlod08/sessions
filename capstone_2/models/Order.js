// Dependencies
const mongoose = require('mongoose');

// Order Schema
const orderSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: [true, 'Please add a user id'],
  },
  product: {
    type: String,
    required: [true, 'Please add a product'],
  },
  totalAmount: {
    type: Number,
    required: [true, 'Please add a total amount'],
  },
  purchasedOn: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = mongoose.model('Orders', orderSchema);
